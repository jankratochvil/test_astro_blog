import { defineConfig } from 'astro/config';

import preact from "@astrojs/preact";

// https://astro.build/config
export default defineConfig({
  site: "https://test-astro-blog-ash882-13bc7fccfadc1052a779fda43d6e3e2d58158bb9.gitlab.io/",
  integrations: [preact()],
  outDir: 'public',
  publicDir: 'static',
});