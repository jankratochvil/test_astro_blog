---
title: 'The Foundation Stone'
pubDate: 2024-05-31
description: 'Do you remember?'
author: 'Astro Learner'
image:
    url: 'https://docs.astro.build/assets/full-logo-light.png'
    alt: 'The full Astro logo.'
tags: ["astro", "blogging", "learning in public", "remember"]
---

## What I've accomplished

1. **Installing Astro**: First, I created a new Astro project and set up my online accounts.

2. **Making Pages**: I then learned how to make pages by creating new `.astro` files and placing them in the `src/pages/` folder.

3. **Making Blog Posts**: This is my first blog post! I now have Astro pages and Markdown posts!

Do you remember everyone?

Tomorrow.

## What's next

I will finish the Astro tutorial, and then keep adding more posts. Watch this space for more to come.