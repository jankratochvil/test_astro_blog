import { defineConfig } from 'astro/config';

import preact from "@astrojs/preact";
import vercel from '@astrojs/vercel/serverless';

// https://astro.build/config
export default defineConfig({
  site: "https://test-astro-blog-opal.vercel.app/",
  integrations: [preact()],
  outDir: 'public',
  publicDir: 'static',
  output: 'hybrid',
  adapter: vercel({
    webAnalytics: { enabled: true }
  }),
});